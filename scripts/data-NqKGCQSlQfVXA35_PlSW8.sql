IF EXISTS(SELECT 1 FROM sys.tables WHERE object_id = OBJECT_ID('Invoices'))
BEGIN;
    DROP TABLE [Invoices];
END;

CREATE TABLE [Invoices] (
    [InvoicesID] INTEGER NOT NULL IDENTITY(1, 1),
    [company] VARCHAR(255) NULL,
    [name] VARCHAR(255) NULL,
    [phone] VARCHAR(100) NULL,
    [email] VARCHAR(255) NULL,
    [country] VARCHAR(100) NULL,
    [amount] VARCHAR(100) NULL,
    PRIMARY KEY ([InvoicesID])
);

INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Praesent Eu Corporation','Cadman Holland','135-0673','nec.quam@icloud.net','France','31''276''380.70'),
  ('Phasellus Incorporated','Ray Wright','1-762-202-8157','proin.non@outlook.net','Germany','1''892''247.57'),
  ('Vel Convallis Industries','Carl Higgins','617-4576','curabitur.vel@icloud.org','Germany','6''930''387.90'),
  ('Sit Amet Associates','Orlando Swanson','1-873-303-4707','pede@hotmail.org','China','31''822''124.01'),
  ('Arcu Vestibulum Ante Associates','Sade Chapman','781-6013','tellus.nunc@google.org','United States','11''290''211.51'),
  ('Nulla In Industries','Ginger Reynolds','433-8485','dui.fusce.aliquam@outlook.com','Germany','59''178''141.69'),
  ('Ipsum Incorporated','Cruz Petty','1-375-774-7815','sapien.aenean.massa@hotmail.couk','United Kingdom','11''310''155.99'),
  ('Ornare Elit Corp.','Veda Martin','553-3202','cras.sed.leo@aol.edu','Italy','32''914''265.23'),
  ('Sem Eget Massa Consulting','Emma Marquez','778-2393','sociis.natoque@outlook.com','Germany','50''814''264.22'),
  ('Metus Vitae Velit Inc.','Carter Norris','263-4575','natoque@google.ca','Italy','52''921''072.05');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Eleifend Egestas Sed Foundation','Deirdre Mcclure','874-1491','faucibus.morbi.vehicula@yahoo.org','Italy','44''372''858.26'),
  ('Turpis Vitae Purus Inc.','Camilla Mcpherson','1-847-483-0243','nulla.at@aol.ca','United Kingdom','6''107''219.26'),
  ('Eu Associates','Ignacia Graham','1-457-986-7436','arcu.vestibulum@protonmail.com','United States','45''206''853.60'),
  ('In Faucibus Orci Corp.','Hall Blair','447-5733','aenean@protonmail.com','Germany','23''749''386.72'),
  ('Orci Phasellus Industries','Hermione Holman','1-534-907-6767','sed@google.org','Italy','71''435''554.21'),
  ('Sed Id Incorporated','Kameko Shelton','721-3973','nisi@aol.couk','Italy','8''333''881.50'),
  ('Aliquam Adipiscing PC','Pearl Kirkland','233-0075','sapien.cras.dolor@icloud.net','France','48''591.80'),
  ('Pharetra Associates','Lionel Walton','583-5744','nulla.integer@yahoo.ca','United Kingdom','1''879''838.02'),
  ('Mauris Ltd','Nina Compton','643-1441','adipiscing.ligula.aenean@icloud.couk','France','83''024''778.68'),
  ('Euismod Ltd','Jordan Curtis','1-878-476-3777','massa.lobortis@icloud.edu','United Kingdom','76''945''322.72');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Mi Eleifend Industries','Kimberly James','1-882-753-0718','vitae.sodales@icloud.net','Italy','44''903''190.33'),
  ('Mus Proin Inc.','Erich Cash','535-7743','tincidunt.orci@google.net','Italy','95''807''927.04'),
  ('Risus Incorporated','Mia Gross','1-344-537-8258','magnis.dis.parturient@protonmail.edu','United Kingdom','37''366''370.03'),
  ('Nec Ante Consulting','Keefe Petersen','1-578-793-6545','sed.dictum.eleifend@yahoo.com','China','2''703''451.35'),
  ('Interdum LLP','Dorothy Weeks','1-575-352-5430','nunc.id@aol.edu','United Kingdom','40''080''856.54'),
  ('Ornare Egestas LLC','Nell House','362-5085','phasellus.libero.mauris@protonmail.net','United Kingdom','97''799''266.16'),
  ('A Tortor Corporation','Emi Reid','1-775-750-6146','elit@aol.couk','United States','26''231''917.72'),
  ('Congue Elit Corp.','Cruz Rich','311-0167','phasellus.ornare@icloud.org','United States','26''162''033.59'),
  ('Integer Sem Company','Vladimir Pitts','1-629-374-1227','non@outlook.edu','Italy','83''264''103.37'),
  ('Eu Sem Incorporated','Price Hunt','1-454-354-3521','odio.aliquam.vulputate@google.net','United Kingdom','73''992''924.12');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Velit Aliquam PC','Tiger Flores','874-1349','donec@hotmail.com','Italy','50''914''844.41'),
  ('Est Mauris Incorporated','Nichole Perez','125-7413','rutrum.lorem@icloud.net','France','36''253''800.22'),
  ('Gravida Sagittis Duis Associates','Rama O''donnell','696-6495','nostra@google.org','Italy','20''443''658.24'),
  ('Orci PC','Darrel Pope','239-2471','laoreet.posuere@aol.org','Italy','97''673''405.31'),
  ('Curabitur Egestas Incorporated','Carolyn Nichols','1-733-432-8617','a.auctor@google.couk','Italy','75''813''888.98'),
  ('Phasellus Dapibus Quam Associates','Cairo Battle','1-968-881-7451','fringilla.mi@hotmail.org','China','70''981''049.61'),
  ('Nunc Ut PC','Velma Perkins','1-594-287-2511','orci.ut@yahoo.net','France','50''005''833.99'),
  ('Morbi Metus Incorporated','Candice Mcbride','935-4975','phasellus@outlook.ca','United States','75''116''583.34'),
  ('Accumsan Corporation','Patricia Hooper','1-967-452-8757','montes.nascetur@yahoo.com','France','37''155''477.44'),
  ('Non Dapibus LLC','Carter Schneider','870-8861','sit@protonmail.couk','France','58''951''215.92');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Et Ultrices Posuere Ltd','Tamekah Saunders','252-2473','vehicula.et.rutrum@hotmail.ca','France','3''378''943.83'),
  ('Dictum Eu Eleifend Foundation','Peter Pate','888-1495','malesuada@protonmail.couk','Germany','23''382''382.70'),
  ('Sem Institute','Hope Mendoza','293-1110','aliquam.gravida@icloud.ca','Italy','99''457''184.77'),
  ('Egestas Aliquam Nec Incorporated','Kirby Robles','1-856-515-1580','suspendisse.dui@yahoo.couk','United States','70''927''945.84'),
  ('Libero Industries','Kadeem Sharpe','1-473-262-7951','porttitor.eros@google.com','United Kingdom','85''971''573.36'),
  ('A Associates','Penelope Gill','1-586-429-4064','curae@yahoo.com','Italy','62''252''231.21'),
  ('Purus Limited','Rigel Mann','615-7889','vivamus.molestie@protonmail.com','Italy','27''243''952.87'),
  ('Ornare Incorporated','Gage Hendricks','1-567-838-2141','diam.duis@protonmail.net','France','9''768''030.10'),
  ('Nunc Industries','Kylee Mccormick','1-539-437-3825','lacus.cras.interdum@google.net','France','68''084''677.47'),
  ('Dictum PC','Grady Calhoun','1-304-168-4736','nonummy.ut@aol.couk','China','81''686''592.63');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Molestie Inc.','Iola Harrell','444-4614','convallis.est@yahoo.edu','Italy','56''807''926.75'),
  ('Eu Metus Associates','Gillian Gray','678-3572','odio@protonmail.org','United Kingdom','93''236''999.76'),
  ('Dictum PC','Salvador Francis','1-475-569-6197','magna.praesent.interdum@aol.com','France','92''654''314.04'),
  ('Tincidunt Dui Augue Foundation','Vance Patton','1-915-717-1863','donec.fringilla.donec@hotmail.com','United Kingdom','38''279''249.52'),
  ('Sit Amet Limited','Xander Leon','945-8174','integer.tincidunt.aliquam@aol.couk','United Kingdom','2''692''173.70'),
  ('Ut Molestie Associates','Gail Rodriquez','473-9825','elementum.at@yahoo.couk','Germany','54''493''530.96'),
  ('Dictum PC','Cooper Schneider','768-4515','eget.metus.eu@hotmail.edu','United States','54''691''292.52'),
  ('Ultricies Ligula Ltd','Vivien Ferguson','1-921-265-7856','neque.sed@outlook.couk','Italy','71''626''314.61'),
  ('Sagittis Lobortis Foundation','Mari Lopez','1-664-758-1614','ut.sem@protonmail.net','France','75''396''196.88'),
  ('Cursus Diam Ltd','Deacon Gregory','817-4982','adipiscing@protonmail.net','United Kingdom','20''510''865.79');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Nullam Feugiat Institute','Brock Camacho','1-819-726-9050','eu.sem@google.edu','United States','90''192''584.46'),
  ('Scelerisque Neque Ltd','Kiara Mercer','753-9858','nam@protonmail.edu','China','39''793''322.28'),
  ('Tortor LLC','Joy Wilder','429-7077','gravida.sagittis@yahoo.org','United States','78''236''947.95'),
  ('Curabitur Ut Incorporated','Cathleen Patel','815-1872','odio.semper.cursus@google.org','United Kingdom','81''507''118.16'),
  ('Facilisis Lorem Tristique Incorporated','Ivor Weber','448-4634','montes@outlook.edu','United Kingdom','21''106''565.78'),
  ('Leo Morbi LLP','Cleo Stevens','1-970-596-7647','netus@yahoo.com','Italy','64''199''854.85'),
  ('Sit Amet Risus Industries','Edan Strickland','778-5942','volutpat@hotmail.org','China','37''044''059.81'),
  ('In Tincidunt Inc.','Reece Foreman','1-401-369-3183','diam@protonmail.com','Italy','71''913''646.33'),
  ('Gravida Corp.','Ronan Silva','1-442-276-8515','mattis.integer.eu@icloud.ca','China','47''855''008.60'),
  ('Eu Inc.','Dominic Martin','1-934-592-5578','libero.integer.in@outlook.ca','United Kingdom','49''719''352.31');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Purus Accumsan Foundation','Castor Fitzgerald','1-818-868-4929','augue@aol.ca','Italy','42''995''424.93'),
  ('Enim Consequat Purus Company','Lunea Walsh','1-688-748-5123','pellentesque.habitant@outlook.org','Italy','91''228''813.61'),
  ('Curabitur Dictum Company','Yasir Black','1-244-497-4284','semper.erat@outlook.com','United States','44''411''233.91'),
  ('Odio Phasellus Company','Tatyana Bender','1-835-711-4177','aliquet@google.org','United States','11''517''129.46'),
  ('Lacus Cras Interdum Consulting','Brian Brennan','879-7696','dolor.fusce@google.edu','Germany','63''752''685.87'),
  ('Elit Curabitur Consulting','Clare Heath','248-3378','cum@hotmail.couk','France','84''347''399.82'),
  ('Vel Turpis Aliquam Incorporated','Leah Moreno','226-9820','magna.nam.ligula@google.couk','Germany','21''353''269.37'),
  ('Vitae Mauris Sit Consulting','Maisie Bolton','1-744-502-6537','sem@google.edu','Italy','99''762''011.94'),
  ('Auctor Odio PC','Beatrice Holt','619-7552','lobortis@aol.com','Italy','96''747''619.27'),
  ('Ac Mattis Velit Limited','Xantha Kramer','508-5284','suspendisse@outlook.net','France','65''163''051.40');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Cursus Vestibulum LLC','Martin Prince','1-325-314-7267','inceptos.hymenaeos@google.org','Germany','69''904''451.06'),
  ('Eu Arcu Corporation','Kylie Garner','1-305-561-4632','sed@icloud.edu','United Kingdom','33''225''649.05'),
  ('Auctor Non Consulting','Emily Flowers','312-6402','justo.eu.arcu@protonmail.org','United Kingdom','23''007''867.16'),
  ('Tincidunt Vehicula Risus LLC','Nell Terry','1-320-783-2528','nisl.arcu@protonmail.com','United Kingdom','76''835''334.40'),
  ('Et Arcu Limited','Guinevere Hewitt','1-914-557-2875','faucibus.orci@hotmail.net','China','37''535''758.72'),
  ('Dolor Sit Consulting','Mari Simmons','225-6570','pretium@aol.org','Germany','4''114''319.18'),
  ('Pharetra Nam Ac Industries','Jameson Merrill','401-1172','etiam@icloud.net','China','29''213''762.09'),
  ('Lobortis Risus Limited','Blossom Ballard','1-470-882-8153','amet.ornare@outlook.org','Germany','24''374''389.05'),
  ('Mauris Sapien Company','Tate Blake','789-6737','et.malesuada@outlook.org','China','76''948''595.97'),
  ('Dictum Ultricies Ligula Industries','Keely Mcdowell','563-0358','aenean@icloud.edu','United Kingdom','88''453''626.22');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Ligula Corp.','TaShya Brock','1-682-478-2163','dui.nec@protonmail.ca','United States','49''626''576.36'),
  ('Mauris Corporation','Gannon Vaughan','1-252-742-1489','in.faucibus@yahoo.edu','China','98''273''895.78'),
  ('A Ltd','Vaughan Pitts','1-314-328-4284','tellus.non@icloud.net','United Kingdom','12''881''726.22'),
  ('Congue Elit Institute','Oliver Velazquez','834-4428','curabitur.vel@protonmail.net','China','91''386''490.59'),
  ('Pede Inc.','Harriet Walters','431-8752','ligula.aenean@protonmail.couk','United States','86''620''787.68'),
  ('Egestas Blandit Inc.','Zoe Herman','712-1658','ut.aliquam.iaculis@hotmail.couk','China','52''118''604.07'),
  ('Elit Aliquam Inc.','Brendan Frazier','837-2364','eleifend@yahoo.ca','France','35''835''782.48'),
  ('Semper Pretium LLC','Hop Mcdonald','581-5575','pretium@outlook.edu','United Kingdom','62''729''992.48'),
  ('Tincidunt Corporation','Branden Tyson','1-520-603-4252','donec.egestas@aol.net','France','72''345''307.66'),
  ('Ut Eros Non Industries','Dieter Good','762-0476','commodo.ipsum@protonmail.net','Germany','99''071''916.07');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Hendrerit Neque Incorporated','Amethyst Russo','1-771-111-6053','in.scelerisque@hotmail.net','United Kingdom','46''433''884.94'),
  ('Proin Mi Consulting','MacKenzie Carney','666-5884','sagittis.nullam.vitae@aol.org','France','55''696''328.95'),
  ('Fusce Mollis Institute','Cain Ayers','1-342-473-4809','tempus@icloud.net','Germany','55''827''987.70'),
  ('Volutpat Ornare Facilisis PC','Maxwell Olson','1-331-529-6234','gravida.aliquam@outlook.couk','China','74''121''187.33'),
  ('Sed Nec Corp.','Dean Orr','435-3423','bibendum.fermentum@hotmail.net','Germany','21''376''676.91'),
  ('Elementum Sem Institute','Daquan Morrow','1-668-830-3419','sodales.at@yahoo.ca','United States','38''831''783.24'),
  ('Euismod Mauris Eu Foundation','Hunter Schroeder','1-222-572-2722','tempor@hotmail.net','China','71''801''393.68'),
  ('Dictum Incorporated','Kennan Norman','627-0938','felis.orci@icloud.org','United Kingdom','17''122''880.11'),
  ('Nascetur Ridiculus Consulting','Erich Freeman','1-313-941-3872','euismod.urna.nullam@icloud.net','United Kingdom','61''117''877.97'),
  ('Sed Sapien Corp.','Sandra Shelton','564-6732','enim.mauris@protonmail.couk','United States','87''255''934.78');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Ligula LLC','Laurel Martinez','1-867-846-4778','tincidunt.adipiscing@google.com','Germany','93''935''317.75'),
  ('Diam Nunc Institute','Gillian Kline','585-6796','morbi@hotmail.ca','China','60''037''481.50'),
  ('Netus Et Malesuada Corporation','Sarah Bernard','1-578-721-6239','turpis@hotmail.com','Italy','86''207''657.08'),
  ('Ultrices Posuere Corporation','Paul Jenkins','1-589-309-1068','sagittis.augue.eu@aol.com','United Kingdom','77''407''944.77'),
  ('Eget Consulting','Felix Bradshaw','1-832-362-5381','rutrum@hotmail.couk','Germany','77''390''867.94'),
  ('Parturient Montes Nascetur Foundation','Athena Holder','1-510-228-5820','at.lacus@icloud.ca','Italy','10''276''959.30'),
  ('Id Ante Incorporated','MacKensie Henry','659-5326','cursus.a@hotmail.ca','France','51''956''670.32'),
  ('Integer Aliquam Adipiscing Institute','Violet Munoz','175-2481','proin.dolor.nulla@protonmail.edu','Italy','52''166''705.15'),
  ('Mi LLP','Neve Mendoza','1-378-458-6047','facilisis.lorem@protonmail.couk','China','71''387''228.53'),
  ('Enim Associates','Dennis Bean','348-4600','egestas@outlook.edu','Germany','67''263''725.53');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('A Scelerisque Incorporated','Justine Bender','438-5686','montes.nascetur@hotmail.net','United States','43''751''387.04'),
  ('Auctor Vitae PC','Flynn Salas','1-363-778-2327','augue.scelerisque@aol.net','Italy','56''801''363.66'),
  ('Dolor Company','Ethan Sims','657-4383','molestie.sed@outlook.net','Germany','16''913''949.03'),
  ('Semper Erat In Corp.','Nita Christian','1-879-893-2674','et@yahoo.ca','Italy','32''869''400.00'),
  ('Tristique Ac Foundation','Clinton Mosley','534-2442','vitae.diam.proin@icloud.org','China','15''392''817.87'),
  ('Malesuada Fames Ac Inc.','Simon Kinney','181-6381','dignissim@outlook.couk','China','49''186''460.43'),
  ('Fusce Aliquam Institute','Stella Mcfadden','1-447-757-4670','egestas.a@google.edu','United Kingdom','27''431''749.34'),
  ('Sit LLC','Abel Carroll','343-0246','orci.in@google.edu','China','49''044''951.91'),
  ('Fermentum Arcu Limited','Marsden Massey','812-4476','sed.sem.egestas@outlook.edu','Italy','3''740''011.84'),
  ('Donec Luctus Institute','Urielle Marquez','1-226-946-8774','magnis.dis.parturient@outlook.edu','United Kingdom','843''758.45');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Natoque Penatibus Et LLC','Iona Montgomery','461-0307','donec.vitae.erat@yahoo.net','China','12''422''392.46'),
  ('Nunc Quisque Limited','Nerea Cline','494-8469','id@outlook.couk','United Kingdom','95''310''464.77'),
  ('Et Netus Et PC','Doris Moore','1-694-108-4152','amet@aol.com','Germany','5''601''952.46'),
  ('Sed Nunc Ltd','Aaron Blair','1-738-461-4671','at@hotmail.edu','Germany','54''637''837.72'),
  ('Tristique Aliquet Phasellus PC','Alvin Estrada','175-3631','faucibus.leo@icloud.com','United States','37''218''338.94'),
  ('Proin Nisl LLC','Damian Owen','826-4228','feugiat@outlook.com','Germany','88''782''278.64'),
  ('Fusce Mollis Incorporated','Cameron Thompson','1-438-281-8572','consequat.enim.diam@hotmail.edu','United Kingdom','21''186''968.54'),
  ('Consequat Lectus Consulting','Brooke Jenkins','1-383-661-2697','posuere@yahoo.net','United Kingdom','89''409''482.98'),
  ('Scelerisque Institute','Penelope Tate','425-1571','faucibus.leo.in@google.net','United States','71''154''074.62'),
  ('Integer Id Limited','Rhonda Chavez','1-747-800-5768','et.malesuada.fames@icloud.org','United Kingdom','83''675''187.53');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Libero Corp.','Idona O''brien','1-921-936-4164','duis@google.net','Italy','88''629''614.15'),
  ('Sapien Cras LLC','Sawyer Curtis','334-8722','nam.porttitor@protonmail.couk','Italy','4''369''169.95'),
  ('Tellus Institute','Kylee Fletcher','261-1615','purus.nullam@aol.couk','Germany','22''230''973.91'),
  ('Facilisis Facilisis Inc.','Gillian Parker','847-2972','tortor.integer@outlook.net','Italy','61''191''895.77'),
  ('Suspendisse Non Corp.','Kasper Guerra','1-655-870-7823','massa@hotmail.net','United Kingdom','83''703''305.31'),
  ('Purus In Industries','Stephanie Walter','924-5770','laoreet.posuere.enim@hotmail.org','United Kingdom','88''828''961.62'),
  ('Sodales Purus Ltd','Nora Shields','642-4087','lorem@protonmail.edu','United Kingdom','89''183''218.06'),
  ('Arcu Aliquam Foundation','Maggie Nicholson','1-643-716-3817','bibendum.donec@yahoo.org','China','77''547''396.68'),
  ('Dolor Donec Foundation','Basil Gamble','777-5258','quis.tristique.ac@google.com','France','67''595''808.68'),
  ('Risus Inc.','Ruby Mckenzie','208-3717','commodo.auctor.velit@google.org','China','62''534''834.23');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Arcu Industries','Holly Hanson','1-846-703-7352','nulla.aliquet@aol.ca','China','38''273''230.54'),
  ('Integer Industries','Malcolm Rhodes','197-8652','adipiscing@outlook.edu','China','79''933''731.52'),
  ('Natoque Penatibus Et Industries','Quin Larson','1-944-848-5036','nec.quam@yahoo.ca','United Kingdom','64''764''698.01'),
  ('Feugiat Institute','Deacon Avery','1-877-451-5847','pede@yahoo.org','United Kingdom','23''638''605.62'),
  ('Integer In LLC','Phelan Haney','1-463-321-9152','lorem.ipsum@yahoo.net','United States','78''274''400.06'),
  ('Lorem Semper Corp.','Sybil Carlson','136-8681','ut.lacus@yahoo.edu','United Kingdom','76''401''998.89'),
  ('Arcu Ac Incorporated','Kato Walker','582-6862','leo.in.lobortis@outlook.couk','China','36''636''613.85'),
  ('Lobortis Mauris LLP','Len Jackson','644-5048','tellus.id@protonmail.edu','United States','38''317''185.28'),
  ('Erat Neque Non Corporation','August Battle','425-9652','sapien@google.com','France','44''146''524.52'),
  ('Aenean Massa Integer Incorporated','Shelly Price','205-6386','non.lobortis.quis@protonmail.net','Germany','16''836''320.35');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Praesent Interdum Foundation','Keefe Navarro','489-1345','feugiat@aol.couk','France','24''086''705.16'),
  ('Mauris Ipsum Associates','Quynn Gilmore','1-214-683-2441','vehicula@outlook.ca','Italy','99''525''486.10'),
  ('Quisque LLP','Haviva Stokes','1-233-516-8180','integer.id.magna@outlook.net','China','7''186''573.39'),
  ('Urna Limited','Christopher Gallegos','1-743-840-3639','mauris@hotmail.edu','France','61''692''056.96'),
  ('Nunc LLP','Ila Rice','1-282-924-6413','pellentesque@aol.net','China','46''371''542.05'),
  ('Faucibus Corp.','Vanna Mathis','1-715-689-5444','sollicitudin.adipiscing@protonmail.com','China','84''373''883.38'),
  ('Enim Mi Ltd','Keiko Horn','306-2325','donec@hotmail.com','China','9''746''228.51'),
  ('In Cursus Incorporated','Travis Richmond','128-3562','laoreet.ipsum@google.com','Italy','58''530''047.71'),
  ('Nisi Cum Sociis Associates','Mira Bates','493-2423','nisi@protonmail.com','United States','3''921''253.39'),
  ('Egestas Institute','Kelly Stewart','1-452-730-9085','morbi.vehicula.pellentesque@yahoo.net','France','17''729''007.07');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Sed Diam Limited','Laith Lucas','1-548-586-2142','dictum.eleifend.nunc@hotmail.couk','France','36''458''939.92'),
  ('Nulla Vulputate Dui PC','Cheryl Sanchez','627-5411','cursus@google.org','France','74''849''567.52'),
  ('Natoque Penatibus Et Corp.','Karyn Leonard','495-3547','vulputate.nisi@outlook.ca','United States','37''933''757.50'),
  ('Magna Malesuada Vel Industries','Orlando Gill','1-626-564-2832','nonummy.ultricies@google.com','Germany','69''827''134.09'),
  ('Dui Associates','Zenaida Fitzpatrick','1-165-668-6176','nunc.mauris@yahoo.net','United States','14''409''625.80'),
  ('Mattis Velit LLC','Laura Langley','1-784-263-7838','est@protonmail.org','France','21''669''397.11'),
  ('Lorem Luctus Ut Company','Brenna Travis','212-8982','phasellus@aol.ca','United Kingdom','63''999''577.92'),
  ('Elementum Dui Quis Institute','Pamela Church','1-511-524-7418','fusce@outlook.org','France','14''117''076.40'),
  ('Enim Mi Associates','Laura Henderson','1-434-591-3668','magnis.dis.parturient@icloud.edu','China','37''632''848.39'),
  ('Iaculis Aliquet Corp.','Flynn Ferrell','1-758-246-4706','velit.pellentesque@icloud.couk','United States','9''754''699.12');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Morbi Neque Limited','Liberty Marshall','1-530-722-3331','pellentesque.sed@outlook.edu','Germany','16''535''276.73'),
  ('Sodales Nisi Magna Inc.','Bethany Hernandez','1-335-825-3217','libero.est@google.couk','France','19''917''247.90'),
  ('Aliquet Inc.','Raja Brooks','363-9491','rutrum@icloud.edu','China','18''902''760.99'),
  ('Nec Luctus Associates','Ginger Gillespie','638-5783','dictum.eu@hotmail.edu','Italy','54''685''326.83'),
  ('Enim Etiam Corporation','Katelyn Haley','723-4274','magna@outlook.net','Italy','43''286''974.72'),
  ('Neque Ltd','Amir Greene','854-4644','faucibus.orci@google.edu','United Kingdom','16''253''034.95'),
  ('Tortor Nibh Corp.','Dennis Santos','185-7143','nunc.ullamcorper.velit@icloud.net','France','84''602''675.43'),
  ('Urna Justo PC','Reece Mcmahon','364-8031','tortor@google.com','Italy','45''878''154.87'),
  ('Elementum Associates','Clarke Sellers','328-7883','luctus.curabitur.egestas@aol.com','China','47''267''821.14'),
  ('Felis Nulla Foundation','Lamar Chandler','811-4815','egestas.lacinia.sed@yahoo.ca','United Kingdom','6''246''752.11');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Elit Dictum Ltd','Lavinia Horne','1-251-819-1557','tempor@google.com','United Kingdom','27''283''301.97'),
  ('Arcu Vestibulum LLC','Oprah Barry','1-606-944-4487','vivamus.molestie@aol.couk','France','26''056''627.96'),
  ('Maecenas Ornare Limited','Michael Pierce','584-7558','sed.orci@aol.edu','China','46''778''057.15'),
  ('Vivamus Nibh Foundation','Lara Hutchinson','1-701-718-6647','auctor.non@aol.org','China','27''914''935.41'),
  ('Nunc Sollicitudin Commodo Foundation','Veda Rosa','811-1453','tortor@yahoo.couk','France','18''715''644.51'),
  ('Fermentum Risus Company','Eve Mullins','1-495-742-7320','ligula.elit.pretium@yahoo.net','United Kingdom','25''114''028.59'),
  ('Vitae Industries','Yasir Martinez','1-464-704-2724','purus.maecenas.libero@hotmail.ca','Italy','10''160''805.74'),
  ('A Foundation','Jonah Jacobson','1-555-119-2732','eu@icloud.ca','France','44''024''321.62'),
  ('Lectus Nullam Corporation','Kameko Chaney','562-2746','bibendum.ullamcorper@protonmail.org','United States','58''874''225.88'),
  ('Auctor Nunc Institute','Graiden Russell','1-403-152-3326','ultrices.posuere.cubilia@outlook.couk','United States','77''055''199.53');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Tempus Risus Incorporated','Ocean Buchanan','836-3643','diam.at@protonmail.org','China','95''836''074.16'),
  ('Adipiscing Lacus Incorporated','Xander Vincent','1-744-284-6191','rutrum@outlook.com','United States','29''145''931.07'),
  ('Ut Dolor Corporation','Piper Espinoza','1-381-402-8746','ipsum@hotmail.edu','Germany','73''788''343.83'),
  ('Fusce Aliquam Enim Foundation','Richard Zamora','931-8278','varius.nam@yahoo.edu','United Kingdom','91''685''603.45'),
  ('Suscipit LLC','Graham Alvarez','526-7876','tempor.augue@aol.org','France','5''952''279.06'),
  ('Turpis Corporation','Thaddeus Mcgowan','633-1712','lectus.ante@google.org','Italy','49''334''831.97'),
  ('Non Lobortis Quis LLC','Lois Justice','1-581-646-6710','nonummy@aol.edu','China','23''998''986.53'),
  ('Molestie Sodales Corp.','Lucy Riggs','1-716-410-8112','aliquet@yahoo.edu','China','93''208''523.46'),
  ('Egestas Sed Industries','Keelie Vincent','1-654-343-5625','risus.nulla@outlook.couk','China','81''290''590.49'),
  ('Quisque Ac Libero Institute','Galena Flynn','1-343-755-4555','arcu.vestibulum@outlook.com','Germany','83''152''178.04');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Imperdiet Nec Consulting','Gwendolyn Benson','1-388-837-8546','sodales.purus@icloud.net','Germany','64''035''551.25'),
  ('Integer Id Incorporated','Claudia Williamson','588-9056','phasellus.dolor.elit@google.couk','China','92''022''187.01'),
  ('Turpis Egestas Fusce Inc.','Clark Browning','172-2312','commodo.tincidunt@yahoo.com','France','12''483''949.11'),
  ('Mus Proin LLC','Jameson Mckay','1-871-691-5436','aliquet@protonmail.edu','Germany','32''661''087.42'),
  ('Ac Nulla LLP','Xaviera Marsh','1-764-859-4361','nunc.sed@google.com','Germany','98''523''128.97'),
  ('Ac Feugiat Non Limited','Ryder Clark','767-0724','id.blandit@google.org','Italy','20''967''395.80'),
  ('Tincidunt Aliquam Arcu Foundation','Basia Dillon','884-9335','amet.lorem.semper@google.org','China','64''543''329.31'),
  ('Maecenas Iaculis LLC','Jocelyn Frost','200-7112','augue.ac@protonmail.ca','United States','17''465''223.74'),
  ('Pretium Neque LLP','Ahmed Davenport','884-3717','aliquam@google.com','China','19''362''976.78'),
  ('Etiam Ltd','Jemima Goodman','871-8182','sed.dictum.eleifend@icloud.ca','Germany','59''766''138.34');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Etiam Institute','Clio Good','875-6453','eget.metus@google.edu','Germany','51''048''145.89'),
  ('Eget Volutpat Ltd','Christopher Rush','1-742-636-4363','in@outlook.couk','Germany','8''720''169.48'),
  ('Ipsum Leo Elementum PC','Darryl Mcdonald','436-7876','donec@protonmail.couk','France','15''362''796.32'),
  ('Mollis Lectus Corporation','Garrett Workman','1-376-553-6962','adipiscing.fringilla.porttitor@aol.org','United States','40''271''418.41'),
  ('Semper Cursus Foundation','Ronan Poole','1-856-827-4662','ante@protonmail.edu','Germany','55''711''881.64'),
  ('Sociis Natoque Corp.','Samson Little','682-7641','massa.quisque.porttitor@yahoo.com','United States','63''687''276.06'),
  ('Malesuada Id Ltd','Daquan Alvarado','1-436-828-2877','lorem.ipsum@outlook.com','United Kingdom','57''893''371.30'),
  ('Orci In Corp.','Nevada Beasley','1-262-825-7237','dignissim.tempor@aol.com','United States','7''526''885.74'),
  ('A Ltd','Clayton Pierce','436-6416','vel.turpis@protonmail.net','France','50''906''191.95'),
  ('Lorem Ipsum Associates','Clarke Gibbs','758-1142','mauris.rhoncus@icloud.org','Germany','31''107''781.39');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Aliquam Erat Volutpat Ltd','Honorato Henry','317-6152','metus.in.nec@aol.ca','France','39''611''950.89'),
  ('Consectetuer Inc.','Levi Spencer','724-8373','vel.convallis@protonmail.edu','China','35''949''394.87'),
  ('Tempus Mauris Consulting','Dominic Chambers','683-5642','a@google.edu','Germany','55''807''822.53'),
  ('Risus Ltd','Brenda Barker','339-4725','dictum@google.org','Italy','52''353''323.23'),
  ('Per Conubia Nostra Institute','Nolan Kane','443-1345','ipsum.non@yahoo.net','United States','75''369''482.26'),
  ('Neque Sed Corp.','Ginger Barton','1-541-386-3711','sem@yahoo.edu','United States','23''241''072.36'),
  ('Rhoncus Proin Institute','Zelenia Weiss','585-0456','odio.tristique@outlook.ca','China','20''214''261.30'),
  ('Dolor Corp.','Allegra Thomas','216-2759','accumsan.sed.facilisis@hotmail.ca','France','60''135''901.03'),
  ('Turpis Nec Foundation','Yael Manning','959-3312','morbi.neque@yahoo.org','United Kingdom','2''543''309.69'),
  ('Eu Odio Limited','Regina Reyes','655-6869','aenean.gravida@google.couk','Italy','76''074''780.03');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Metus In Lorem PC','Jarrod Carver','1-566-736-3114','volutpat.nulla@outlook.ca','France','34''994''589.97'),
  ('Tellus LLC','Jack Mason','1-228-802-8442','dolor@icloud.couk','United Kingdom','80''886''991.94'),
  ('Malesuada Corp.','Suki Holt','946-6378','et.magnis@protonmail.com','United States','29''301''142.52'),
  ('Ac Risus Ltd','Molly Rocha','1-378-529-5736','non.hendrerit@protonmail.com','Germany','68''846''816.64'),
  ('Aliquam Institute','Nina Sutton','298-4850','nonummy@hotmail.com','France','53''841''611.06'),
  ('Nisi A Odio Inc.','Shelby Dillon','1-576-786-2875','convallis.dolor@hotmail.couk','China','15''666''754.00'),
  ('Nascetur Ridiculus Foundation','Medge Mccray','315-8101','porttitor.interdum.sed@hotmail.net','France','37''386''802.25'),
  ('Porttitor Vulputate PC','Irene Mathews','1-525-719-7335','aliquet.molestie@aol.ca','France','67''019''099.61'),
  ('Consectetuer Adipiscing Corp.','Holly Acosta','758-8614','quis@hotmail.net','China','30''615''756.79'),
  ('Ipsum Limited','Hammett Chaney','1-368-335-3252','sed.tortor.integer@icloud.net','United Kingdom','71''396''306.68');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Ut Lacus Limited','Melvin Barnett','233-8337','ac.mi@hotmail.couk','United Kingdom','46''604''794.48'),
  ('Sem Elit LLC','Hiroko Floyd','433-3688','sodales@hotmail.org','United Kingdom','63''580''214.14'),
  ('Dolor Egestas Foundation','Roth Galloway','549-7723','scelerisque.dui@protonmail.org','France','99''461''289.50'),
  ('Mauris Eu PC','Plato Callahan','863-3753','nibh.lacinia.orci@hotmail.com','France','79''723''068.19'),
  ('Magna Inc.','Eric Mercado','1-477-465-6641','erat.neque@google.com','China','89''994''880.78'),
  ('Velit Sed Malesuada LLC','Cherokee Greene','148-3471','non.dapibus@google.ca','China','69''823''023.86'),
  ('Facilisis Facilisis Industries','Macey Wyatt','284-7894','nibh.enim.gravida@google.edu','Italy','33''320''813.32'),
  ('Donec Est Mauris Corporation','Florence Gross','241-3721','ornare.in@aol.edu','United Kingdom','67''887''772.64'),
  ('Donec Nibh Institute','Jarrod Young','1-727-267-5380','aptent@icloud.ca','Italy','76''157''537.14'),
  ('Lacus Quisque Industries','Kalia Conrad','1-284-553-4692','amet@hotmail.couk','France','10''845''844.86');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Non Associates','Ella Hill','1-468-614-0766','sem@google.ca','France','91''984''801.76'),
  ('Arcu Et Company','Miranda Dunn','583-1311','cras.sed@outlook.edu','United Kingdom','4''587''989.29'),
  ('Odio LLC','Camille Elliott','911-6451','venenatis.vel.faucibus@protonmail.com','France','93''753''486.52'),
  ('Nibh Aliquam Institute','Chiquita Fleming','1-567-758-8370','rhoncus.proin@icloud.net','France','39''833''872.07'),
  ('Ac LLC','Deacon Noel','883-0886','ridiculus.mus@google.net','Germany','60''659''958.11'),
  ('Non Quam Foundation','Nerea Campbell','568-6769','pellentesque.ultricies@hotmail.net','China','90''438''250.50'),
  ('Convallis Dolor Associates','Griffin Sloan','353-8622','porttitor.vulputate@google.net','Germany','66''501''735.11'),
  ('Fermentum Fermentum Arcu Corp.','Zena Cunningham','414-7006','id.magna@protonmail.org','China','6''501''350.00'),
  ('Erat Volutpat Nulla Ltd','Zena Knapp','1-770-263-7422','elit.aliquam@icloud.edu','United States','29''806''957.79'),
  ('Faucibus Limited','Marah Stafford','509-3427','sagittis.semper@icloud.couk','Italy','91''827''538.97');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Morbi Corp.','Guy Workman','1-530-988-2264','sodales.nisi@icloud.couk','United States','59''756''837.69'),
  ('Eget Lacus Inc.','Quintessa Rocha','667-7157','sed@outlook.ca','China','73''623''729.86'),
  ('Curabitur Ut Ltd','Courtney Juarez','516-2748','cras@yahoo.org','China','95''153''754.92'),
  ('Eros Nec Industries','Ronan Ramirez','1-137-456-2231','magna.malesuada@google.couk','Germany','8''694''962.85'),
  ('Magnis Dis Parturient Ltd','Ralph York','1-289-413-8413','tincidunt.vehicula@google.org','Germany','51''610''831.50'),
  ('Dui In Company','Kirestin Ray','585-2718','lorem.donec@google.edu','Italy','17''801''622.72'),
  ('A Sollicitudin Associates','Ryder Simon','1-245-355-3577','aliquam@yahoo.couk','China','18''497''172.48'),
  ('Ac Mattis LLC','Dahlia Mckenzie','897-7414','nullam.scelerisque.neque@icloud.edu','Germany','37''581''456.23'),
  ('Id Ante Associates','Cathleen Sweet','868-7200','urna.suscipit@icloud.edu','Italy','31''333''472.69'),
  ('Ullamcorper Duis Cursus Foundation','Clare Duffy','1-481-729-8435','sapien.cursus.in@icloud.edu','Germany','10''456''816.97');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Mauris Foundation','Ebony Fulton','565-3678','risus@protonmail.com','United States','8''695''487.46'),
  ('Est Nunc Company','William Gaines','1-866-793-3153','suscipit.est@hotmail.net','United Kingdom','81''095''973.93'),
  ('Donec Tincidunt Donec Ltd','Jameson Horton','514-1751','felis.purus@yahoo.couk','Germany','71''211''492.50'),
  ('Mauris Elit Corporation','Aaron Cash','1-828-461-5850','cum.sociis@google.edu','China','40''863''349.33'),
  ('Odio Ltd','Ryan Joyce','1-306-446-2849','pellentesque.tellus@hotmail.couk','France','86''935''396.27'),
  ('Quisque Fringilla Institute','Wade Morris','1-513-428-8798','lectus.nullam@aol.couk','China','76''094''436.78'),
  ('Adipiscing Inc.','Margaret Callahan','371-4273','duis.gravida@google.couk','Italy','92''834''950.16'),
  ('Velit Cras Incorporated','Kenyon Clemons','404-2615','velit@hotmail.net','France','50''461''402.92'),
  ('Tortor Integer Aliquam Foundation','Andrew Bean','1-280-675-5745','duis.elementum@protonmail.edu','United Kingdom','46''994''421.11'),
  ('Varius Industries','Nathaniel Christensen','1-686-621-2292','orci.lobortis@outlook.org','China','34''742''773.81');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Consectetuer Inc.','Clinton Mckinney','1-534-966-0417','malesuada@aol.org','United Kingdom','51''839''085.63'),
  ('Ultrices Duis Corporation','Holmes Rodriguez','1-854-994-4058','cras.dolor@hotmail.couk','China','72''951''332.63'),
  ('Molestie Dapibus Institute','Arthur Vinson','191-3194','duis@hotmail.edu','France','26''054''392.45'),
  ('Erat Vivamus Nisi Industries','Lavinia Hooper','236-9487','at.velit@hotmail.net','China','71''318''065.31'),
  ('Nisl Sem Ltd','Zachery Mccarty','1-737-715-8127','quisque@aol.ca','China','43''454''074.80'),
  ('Eget Company','Meghan Middleton','1-333-641-6117','ac.facilisis@yahoo.net','United States','59''948''185.28'),
  ('Laoreet Incorporated','Macy Valenzuela','1-472-885-1027','sagittis.nullam@aol.couk','Italy','40''661''566.68'),
  ('Aliquam PC','Ursula Cooke','926-8433','parturient@aol.net','France','53''691''613.33'),
  ('Vitae Erat Vivamus Institute','Aquila Peterson','1-489-262-9523','ac.orci@google.net','Germany','17''710''937.95'),
  ('Sed Corp.','Reed Roy','775-2623','sit.amet@aol.ca','Germany','67''874''652.77');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Facilisis Lorem LLC','Lawrence Mcknight','372-8798','adipiscing@aol.couk','Italy','425''241.98'),
  ('Egestas Blandit PC','Briar Yates','1-819-627-1145','aenean.eget.magna@yahoo.com','China','31''994''813.86'),
  ('Aliquet Sem Ut Associates','Adria Anderson','367-4421','nisi.magna.sed@google.ca','China','15''965''872.64'),
  ('Urna Suscipit Ltd','Zenia Quinn','1-697-298-6571','et.rutrum@protonmail.org','United States','29''819''321.46'),
  ('Luctus Aliquet Inc.','Orla Gonzales','434-3559','dolor@icloud.net','Germany','86''740''910.59'),
  ('Gravida Sagittis Company','Lilah Chen','901-6448','et@protonmail.edu','Germany','3''225''189.33'),
  ('Fringilla Porttitor Vulputate Corporation','Holly Warren','1-247-289-4374','in@hotmail.ca','United Kingdom','23''659''770.12'),
  ('Ac Ltd','Guy Bullock','1-561-463-8015','suspendisse@aol.com','Italy','50''767''263.69'),
  ('Et Magnis Dis LLP','Caldwell Blanchard','1-524-270-4027','imperdiet@icloud.ca','Italy','70''330''118.55'),
  ('Sodales Limited','Zelda Cleveland','1-796-311-8826','dictum.placerat@hotmail.net','France','73''113''347.55');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Eleifend Nunc Risus Incorporated','Ivan Stafford','510-2847','primis.in@hotmail.edu','Italy','94''007''173.47'),
  ('Lobortis LLP','Gregory Meyers','1-833-319-5284','penatibus@google.org','Germany','66''238''619.36'),
  ('Dignissim Tempor Arcu Incorporated','Alexis Benson','478-7225','risus.nunc.ac@hotmail.ca','United Kingdom','36''684''145.97'),
  ('Phasellus Elit Pede LLP','Clark Navarro','1-767-632-7528','blandit.at@hotmail.edu','Germany','11''041''465.05'),
  ('Est Mauris LLC','Jaden Coffey','1-333-670-2565','diam.nunc.ullamcorper@outlook.ca','France','18''551''739.91'),
  ('Amet Orci Ut Institute','Francis Kim','1-668-489-4578','egestas.a@icloud.couk','China','54''774''214.83'),
  ('Et Lacinia Institute','Lewis Bruce','1-626-119-0565','velit@aol.com','China','73''423''064.50'),
  ('Gravida Incorporated','Seth Albert','648-5197','sed.eu.eros@outlook.com','China','43''728''893.14'),
  ('Gravida Sagittis Limited','Olivia Carson','1-672-903-5612','tellus.non@hotmail.org','China','81''497''493.32'),
  ('Tellus Corp.','Catherine Barron','961-2167','vel.pede.blandit@yahoo.com','Germany','93''970''500.19');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Penatibus LLC','Connor Harris','1-659-478-3736','euismod@yahoo.org','United Kingdom','84''408''765.06'),
  ('Ipsum Nunc Id Limited','Ross Gray','1-473-757-4865','mollis.nec@yahoo.edu','Germany','60''868''294.34'),
  ('Etiam Ligula LLC','Justin Mckee','762-8770','erat.sed.nunc@protonmail.com','United States','9''821''902.50'),
  ('Vitae LLP','Michelle Singleton','209-9250','sit.amet.ornare@icloud.org','China','11''133''076.72'),
  ('Justo Eu Arcu Inc.','Shaine Randall','770-9258','nec.euismod.in@hotmail.org','United Kingdom','92''079''989.64'),
  ('Sed Inc.','Scarlett Flores','1-634-346-6634','leo.vivamus@protonmail.couk','China','8''774''318.87'),
  ('Vivamus Sit Corp.','Jada Barker','1-293-447-0634','sem.vitae@google.net','Germany','78''270''310.28'),
  ('Pharetra Ut Foundation','Justin Hess','747-8024','proin.nisl.sem@outlook.com','France','86''120''765.78'),
  ('Risus Quisque Company','Jamal Rice','244-8315','ligula.aenean@icloud.couk','United States','63''962''948.90'),
  ('Gravida Non Associates','Trevor Maxwell','1-352-843-0768','luctus.lobortis@icloud.couk','China','46''593''988.90');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Etiam PC','Oren Howell','1-637-518-7517','rhoncus@google.couk','Germany','3''653''397.09'),
  ('Id Sapien Institute','Oren Moss','1-829-247-7537','varius.nam@hotmail.ca','United Kingdom','77''871''398.58'),
  ('Cursus Et Foundation','Quintessa Vega','1-776-941-5864','pede.nec@hotmail.org','Germany','63''507''837.93'),
  ('Gravida Sagittis Institute','Macy Henson','1-753-506-4595','enim.suspendisse.aliquet@yahoo.ca','Italy','77''933''034.46'),
  ('Dictum Ultricies Ligula Associates','Blaze Hickman','632-3583','phasellus.nulla.integer@outlook.net','Germany','13''039''479.46'),
  ('Ridiculus Mus Industries','Brian Gray','722-4346','quis.pede@yahoo.edu','China','90''025''588.06'),
  ('Curae Phasellus Inc.','Stewart Reeves','185-7671','elit@hotmail.net','China','31''066''511.25'),
  ('Scelerisque LLC','Alisa Goodwin','1-476-248-1865','risus.in@protonmail.ca','France','53''767''287.65'),
  ('Vestibulum Ante Ipsum Corporation','Colette Hooper','1-424-692-6405','in@outlook.edu','Germany','4''776''473.79'),
  ('Purus Ac Industries','Vaughan Gutierrez','758-3572','malesuada.integer@icloud.org','China','62''329''544.26');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Arcu Vel Associates','Merritt Barber','454-7659','blandit.congue@outlook.org','Germany','58''537''517.45'),
  ('Pellentesque Ut Ipsum Limited','Jescie Rocha','1-736-199-2039','eu.dui.cum@yahoo.ca','United Kingdom','14''572''642.71'),
  ('Non Ante Bibendum Ltd','Cade Petersen','752-6552','mauris@icloud.com','China','99''397''675.53'),
  ('Turpis LLC','Emily Hood','137-6715','pulvinar.arcu.et@protonmail.org','United States','26''368''681.44'),
  ('Molestie Orci Tincidunt Industries','Iola Bray','887-2578','aliquam.eu.accumsan@protonmail.ca','China','10''324''239.54'),
  ('Molestie Arcu Associates','Geoffrey Bryant','1-969-767-6083','mattis.integer@outlook.ca','Italy','12''041''070.55'),
  ('Non LLC','Lee Walter','812-5204','sapien@aol.org','Germany','81''086''653.07'),
  ('Sapien Imperdiet Limited','Stacey Buchanan','1-965-389-3456','libero@outlook.edu','Italy','84''415''933.82'),
  ('Luctus Curabitur Egestas Inc.','Kasimir Barry','301-1522','quis.lectus.nullam@outlook.com','Germany','98''471''268.96'),
  ('Orci Quis Corporation','Thaddeus Gibson','1-967-345-5590','donec.felis.orci@outlook.ca','United Kingdom','9''229''537.43');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Tortor Dictum Corporation','Burton England','1-343-998-1506','cras.dictum@google.edu','Germany','23''167''221.55'),
  ('Libero At Auctor Industries','Liberty Bray','1-817-428-7580','magna.nam.ligula@outlook.com','United States','73''416''252.30'),
  ('Porttitor Tellus Non Corp.','Kuame Cherry','1-348-246-2223','aliquam@yahoo.couk','France','72''069''699.07'),
  ('Mauris PC','Gil Houston','1-205-945-3298','auctor.vitae@google.com','United States','99''168''383.72'),
  ('Arcu Vestibulum Institute','Harrison Horne','244-6521','turpis@protonmail.org','Germany','57''737''331.37'),
  ('Porta Ltd','Lionel Mercado','443-3684','morbi.tristique@outlook.ca','Germany','67''094''371.71'),
  ('Eu Tellus Corporation','Linda Morrison','1-150-436-5287','auctor.mauris@yahoo.net','Germany','85''508''852.71'),
  ('Nisl Quisque Foundation','Gannon Velazquez','385-5568','quisque.tincidunt.pede@icloud.org','Italy','94''119''546.90'),
  ('Malesuada Vel Limited','Kessie Elliott','1-265-733-5592','non.egestas.a@protonmail.com','France','15''056''916.17'),
  ('Fringilla Donec Foundation','Guinevere Wright','1-393-567-0257','in.hendrerit@icloud.org','Italy','99''261''609.94');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Sollicitudin Adipiscing Ligula Limited','Iona Nolan','867-1675','cras@protonmail.org','United States','75''927''651.62'),
  ('In Limited','Alexandra Beck','1-377-545-4327','nec.tempus@aol.org','Italy','26''442''880.67'),
  ('Sem Eget Massa Industries','Tucker White','1-201-553-5666','venenatis.vel.faucibus@outlook.org','China','15''516''524.54'),
  ('Ornare Institute','Colleen Gardner','1-951-338-0471','nullam.enim@icloud.com','United Kingdom','7''955''463.43'),
  ('Vitae Corp.','Darrel Valenzuela','1-781-256-3123','sit.amet.consectetuer@google.edu','China','88''770''330.27'),
  ('Curabitur Egestas Industries','Aphrodite Gilliam','1-792-592-9755','cum@google.couk','United Kingdom','87''701''085.50'),
  ('Neque Vitae Institute','Illana Young','1-502-457-9994','consectetuer.euismod@yahoo.edu','United Kingdom','825''786.87'),
  ('Lobortis Tellus Justo Industries','Quynn Perkins','1-286-847-7444','aenean.gravida@hotmail.net','Italy','83''658''460.41'),
  ('A Nunc Corporation','Kareem Acevedo','276-9586','ipsum.ac.mi@yahoo.edu','China','17''530''069.94'),
  ('Arcu Eu LLC','Eagan Thompson','1-564-530-4373','proin@hotmail.ca','France','11''063''242.33');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Nec Tempus Consulting','Brenda Haley','1-386-252-4464','ante.blandit@outlook.ca','France','44''776''577.38'),
  ('Ornare Sagittis Corp.','Ivory Aguirre','938-3374','metus@hotmail.ca','France','69''127''295.88'),
  ('Lorem Inc.','Mariam Mcknight','204-2946','nulla@google.net','Germany','72''057''567.82'),
  ('Ultrices Posuere Foundation','Walter Ross','1-613-388-8378','eget.nisi@yahoo.org','China','98''887''840.39'),
  ('Aliquet Sem Ut LLC','Yeo Henderson','1-376-463-3663','lorem.auctor.quis@outlook.com','China','35''818''392.86'),
  ('Consequat Purus Maecenas Incorporated','Savannah Fitzgerald','1-694-652-5859','tristique.aliquet@aol.edu','Germany','24''410''662.06'),
  ('Ultricies Dignissim Lacus Inc.','Nero Fry','604-9479','nullam.vitae@icloud.ca','United States','65''242''521.25'),
  ('Ipsum Corp.','Marah Clayton','560-7742','turpis@icloud.couk','China','58''218''140.31'),
  ('Dolor LLP','Rowan Lott','854-3500','integer.aliquam@protonmail.ca','United Kingdom','65''176''972.48'),
  ('Ligula Tortor Institute','Rana Waller','1-129-890-6928','orci.tincidunt@google.net','Italy','87''587''415.72');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Orci Adipiscing Non Foundation','Zephania Fernandez','988-7736','velit@icloud.com','France','58''252''001.65'),
  ('Elit Ltd','Christopher Lane','1-828-245-6455','dolor.vitae.dolor@outlook.com','Italy','94''851''287.91'),
  ('Ac Incorporated','Malcolm Wise','1-258-565-5141','nec.tempus.mauris@yahoo.ca','Italy','9''234''212.80'),
  ('Vitae Mauris LLP','Jescie Barr','1-478-394-1160','eros.nam.consequat@outlook.couk','United States','25''193''348.31'),
  ('Cras Eget Consulting','Riley Torres','1-468-357-4941','nunc.risus@yahoo.net','China','95''992''331.27'),
  ('Habitant Inc.','Travis Keith','1-341-878-6685','semper.egestas.urna@aol.couk','United States','6''402''044.22'),
  ('Quisque Inc.','Calvin Hansen','1-421-226-2338','in.condimentum.donec@protonmail.com','United Kingdom','70''670''251.57'),
  ('Proin Eget Associates','Keith Stuart','542-1989','aliquam.ultrices.iaculis@yahoo.edu','United States','7''977''219.95'),
  ('Lorem Ipsum LLC','Colin Kline','1-638-345-9851','ultricies.dignissim@yahoo.org','France','88''928''379.38'),
  ('Faucibus Id Libero Industries','Arden Harrington','1-975-756-7093','nulla@hotmail.edu','United States','36''595''315.88');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Sapien Nunc PC','Zachary Rivera','1-834-932-5176','urna@outlook.com','Germany','74''080''878.04'),
  ('Vulputate Eu Odio Associates','Jolie Orr','498-1532','est.mauris@icloud.com','China','62''756''904.38'),
  ('Pretium Et LLC','Allistair Hahn','254-2313','lobortis.augue@aol.com','United States','97''528''739.28'),
  ('Facilisis Eget Incorporated','Anne Roth','645-2312','tincidunt.donec@google.org','United Kingdom','65''230''568.60'),
  ('Sed Eget Lacus Associates','Summer Good','880-5483','in.scelerisque.scelerisque@icloud.com','United States','47''945''938.74'),
  ('Sed Foundation','Simon Mcdowell','708-5649','donec.tempus.lorem@yahoo.net','Germany','64''333''983.55'),
  ('Scelerisque Industries','Nadine Walker','1-856-953-4884','quisque.varius.nam@protonmail.net','United States','13''503''618.76'),
  ('Erat Institute','Laith Phelps','1-573-392-4109','integer@icloud.com','Italy','78''442''122.54'),
  ('Cursus Et Associates','Ivana Meadows','1-250-377-1362','nec@aol.org','China','95''459''380.28'),
  ('Egestas Hendrerit LLC','Wade Rose','1-453-681-6014','dapibus.ligula@protonmail.edu','Italy','81''361''523.77');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Per Conubia Nostra LLP','Jaden Cole','541-1745','ligula.nullam@icloud.com','United Kingdom','2''835''336.86'),
  ('Ornare In Corp.','Nayda Luna','1-888-824-4348','eu.arcu.morbi@outlook.com','Germany','29''354''492.28'),
  ('Sapien Gravida Non Corporation','Neville Frank','358-1948','aliquam.gravida.mauris@outlook.com','Germany','51''790''633.60'),
  ('Nullam Corporation','Ina Baird','1-247-124-3631','litora@hotmail.net','United Kingdom','16''521''468.60'),
  ('Aliquam Eros LLC','Tate Armstrong','1-472-863-7313','nisl@yahoo.ca','Germany','84''048''989.97'),
  ('Odio Etiam LLC','Davis Melton','151-0469','fusce@hotmail.edu','Italy','5''540''959.95'),
  ('Turpis Nulla Aliquet Industries','Zeph Rasmussen','646-0112','nec.urna@hotmail.ca','China','9''870''110.64'),
  ('Semper Cursus Corp.','Hope Farrell','1-607-112-3183','nulla.vulputate.dui@aol.com','China','70''285''486.37'),
  ('Lorem Tristique Foundation','Maia Dennis','1-236-454-5544','non.vestibulum.nec@yahoo.edu','China','14''751''610.13'),
  ('Ipsum Porta Elit Foundation','David Ruiz','166-8634','sit.amet.dapibus@hotmail.couk','France','67''719''352.81');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Fusce PC','Freya Albert','339-2475','aenean.massa@aol.org','China','93''078''630.77'),
  ('Vehicula Et Foundation','Merrill Bernard','318-3749','dis.parturient@icloud.net','Germany','57''491''295.79'),
  ('Velit Ltd','Erasmus Mason','888-8958','venenatis.vel.faucibus@hotmail.com','Germany','61''588''288.92'),
  ('Blandit Viverra Associates','Lacota Hicks','1-245-330-4627','mattis.ornare@hotmail.couk','France','48''485''965.99'),
  ('Nunc Limited','Chelsea Simon','1-603-778-7255','nisl.sem.consequat@icloud.org','Italy','10''048''956.97'),
  ('Dolor Donec Fringilla Associates','George Tucker','564-0529','nisi.nibh.lacinia@aol.net','France','44''938''600.34'),
  ('Sed Dictum Eleifend Incorporated','Hunter Lawrence','1-771-858-3514','scelerisque.sed.sapien@hotmail.com','China','71''463.13'),
  ('Dignissim Maecenas Limited','Steven Casey','454-7624','scelerisque.neque@icloud.com','United States','39''428''785.46'),
  ('Quisque Nonummy Ipsum Consulting','Willow Christensen','634-7813','mi@yahoo.com','United Kingdom','84''310''368.62'),
  ('Lectus Convallis Industries','Bell Johnson','1-777-702-6594','mollis.non.cursus@outlook.org','Italy','25''763''830.53');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Etiam Laoreet Corp.','Clio Juarez','1-366-931-8630','ipsum.donec@aol.net','France','58''892''165.13'),
  ('Nunc Nulla Limited','Hillary Lyons','674-4125','aliquam@protonmail.net','Germany','46''837''325.50'),
  ('Nec Diam Duis Associates','Freya Goodman','945-4145','gravida.sagittis.duis@yahoo.net','Italy','97''641''824.25'),
  ('Sem Ut Institute','Lyle Kemp','1-420-736-3534','consectetuer.adipiscing.elit@hotmail.couk','United Kingdom','4''954''718.21'),
  ('At Lacus Institute','Asher Perez','476-9034','ligula@hotmail.edu','Germany','15''463''245.39'),
  ('Nibh Industries','Yasir Velazquez','1-888-755-3582','mi@hotmail.couk','China','61''717''794.90'),
  ('Fermentum Metus Institute','Chanda Sellers','1-715-463-4762','nullam.enim@protonmail.org','France','56''540''116.93'),
  ('Phasellus Dapibus Quam Inc.','Patrick Klein','791-5506','rutrum.urna.nec@icloud.net','Italy','27''300''574.68'),
  ('Sed Inc.','Blaze Knight','1-971-904-3862','nibh@aol.edu','United Kingdom','37''005''126.93'),
  ('Nec Tempus Foundation','Bernard Dodson','1-585-924-3424','est.ac.mattis@protonmail.couk','France','27''950''940.31');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Enim Diam Industries','Ursula Bartlett','1-822-958-5155','nascetur.ridiculus@outlook.net','France','18''422''086.30'),
  ('Tristique Pharetra Quisque Industries','Griffin Buckner','881-5574','convallis@outlook.couk','Germany','66''324''809.45'),
  ('Sed Dui Limited','Karyn Byers','852-1231','nisi.magna.sed@aol.net','Germany','42''187''701.93'),
  ('Et Limited','Russell Savage','1-849-943-3153','quisque@aol.couk','Germany','14''696''390.09'),
  ('Auctor Corp.','Lareina Blake','1-631-673-7240','odio@yahoo.net','Germany','64''058''297.39'),
  ('Dictum Augue Associates','Galvin Parker','431-2222','eu.tellus@icloud.couk','France','65''719''053.50'),
  ('Vel Ltd','Emery Butler','865-8096','laoreet.libero.et@protonmail.couk','United Kingdom','7''321''150.59'),
  ('Faucibus Lectus LLP','Eve Browning','431-2862','leo.elementum@icloud.net','France','54''648''717.23'),
  ('Aliquam Nisl Nulla PC','Hu Peck','293-3238','eget.metus@hotmail.ca','United States','69''191''534.80'),
  ('Varius Et Inc.','Hasad Bradley','263-7582','phasellus.in@aol.net','China','87''745''412.14');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Velit Eget Company','Nora Atkinson','1-550-681-6885','a.aliquet.vel@outlook.org','Italy','94''910''625.63'),
  ('Fermentum Fermentum Industries','Renee Knox','676-0811','justo.eu@icloud.net','Germany','29''288''441.86'),
  ('Fringilla Euismod Institute','Rebecca Barron','1-352-805-4537','leo.vivamus@yahoo.net','Italy','84''630''099.80'),
  ('Tortor Dictum Ltd','Kristen Cohen','421-0656','lectus.convallis@aol.ca','United States','18''256''277.11'),
  ('Dapibus Quam Foundation','Leroy Conway','1-382-583-5105','vitae.dolor@yahoo.edu','Germany','45''342''791.70'),
  ('Rutrum Urna Foundation','Tyler Jefferson','1-389-642-4630','ac.arcu.nunc@outlook.edu','United States','16''347''661.59'),
  ('Netus Et Foundation','Madaline Gibson','655-7445','metus@yahoo.ca','France','33''828''201.16'),
  ('Orci Sem Limited','Eleanor Rush','886-6496','in.tempus@icloud.com','United Kingdom','7''589''110.12'),
  ('Risus Donec Nibh Limited','Avram Stephenson','227-1150','tellus@outlook.org','United States','75''995''554.30'),
  ('Sapien Institute','Xyla Leonard','222-8956','amet.consectetuer@protonmail.com','United States','69''824''484.27');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Lacus Institute','Tanisha Gaines','1-358-756-4500','imperdiet.non@icloud.org','United States','22''002''430.43'),
  ('Ut Pharetra Sed Consulting','Gregory Clarke','771-2171','diam.dictum.sapien@protonmail.couk','Germany','75''885''350.62'),
  ('Parturient Montes Associates','Wyoming Mccullough','886-1654','tincidunt.adipiscing@icloud.edu','United States','90''987''344.80'),
  ('Eu Arcu Corp.','September Woodward','218-7350','vitae@google.net','United Kingdom','86''831''944.34'),
  ('Ligula Eu Institute','Ora Preston','1-341-359-8785','etiam@aol.edu','United Kingdom','76''897''010.43'),
  ('Dolor Industries','Kirk Sandoval','1-260-482-3338','non@aol.ca','United Kingdom','80''710''037.47'),
  ('Mauris Molestie Pharetra Foundation','Patricia Gallegos','1-721-867-7582','at.fringilla.purus@hotmail.com','United Kingdom','3''493''674.34'),
  ('Adipiscing PC','Josephine Albert','922-6747','consectetuer.adipiscing.elit@icloud.ca','Germany','49''454''471.72'),
  ('Sem Nulla Industries','Holmes Faulkner','969-5117','quam.dignissim@hotmail.org','United Kingdom','9''281''187.80'),
  ('Sit Amet Inc.','Petra Marquez','1-648-413-8774','pharetra.felis.eget@outlook.edu','United Kingdom','12''894''380.77');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Eu Eleifend Nec PC','Noah Bernard','113-4858','sit.amet.risus@hotmail.org','China','34''115''959.48'),
  ('Id Sapien PC','Logan Sykes','955-9376','diam.sed.diam@aol.couk','France','30''088''397.85'),
  ('Turpis Non Institute','Sean Norris','153-0527','nunc.mauris@hotmail.org','Italy','18''575''338.34'),
  ('Mauris Sapien Industries','Ferdinand Gibbs','1-918-591-8596','posuere.enim.nisl@aol.ca','Germany','93''718''126.46'),
  ('Risus Corp.','Ezekiel Roman','719-3896','placerat.eget@aol.net','United States','89''433''655.60'),
  ('Pede Malesuada Vel PC','Chastity Pearson','155-3274','pede.praesent@icloud.ca','China','84''738''026.47'),
  ('Cubilia Curae Inc.','McKenzie Sawyer','1-377-555-4266','sit.amet@yahoo.com','Italy','55''290''767.90'),
  ('Sit Amet Institute','Sierra Galloway','1-486-152-1453','elit@hotmail.couk','China','92''719''922.01'),
  ('Per Conubia Industries','Seth Sims','1-973-468-7129','mi.aliquam.gravida@icloud.com','Italy','99''160''822.08'),
  ('Arcu Imperdiet Incorporated','Amethyst Campos','343-1866','nibh@aol.ca','Italy','42''790''191.73');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Dapibus Rutrum Institute','Kelly Elliott','228-9276','donec.porttitor.tellus@hotmail.net','France','59''552''971.84'),
  ('Enim Mi Corp.','Jamalia Dorsey','1-638-684-1802','cras.eget@outlook.net','France','34''623''779.63'),
  ('Erat Vivamus PC','Maggy Everett','469-3427','duis.cursus@yahoo.com','Germany','39''436''718.63'),
  ('Consectetuer Mauris Id Associates','Perry Conway','1-824-480-6830','vitae.aliquet@protonmail.com','United States','32''054''978.04'),
  ('Auctor Company','Kareem Koch','1-854-297-2447','adipiscing.elit.aliquam@google.com','Germany','18''310''718.80'),
  ('Nisi Corporation','Uta Price','1-488-786-2867','nonummy.ultricies@aol.couk','France','96''601''212.73'),
  ('Neque Limited','Brody Morin','274-3349','dolor.sit@icloud.ca','China','19''021''621.64'),
  ('Urna Inc.','Carissa Fitzgerald','822-9120','eros.proin@google.com','United States','56''241''266.65'),
  ('Lectus Nullam Corporation','Clayton Berg','1-762-154-6475','eget@aol.net','Italy','3''771''293.41'),
  ('Morbi Accumsan Laoreet PC','Joel Hamilton','373-5235','gravida.praesent@icloud.edu','Germany','53''727''939.83');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Ullamcorper Nisl Incorporated','Dahlia Mclean','1-283-570-2046','ut.eros@outlook.com','France','77''170''572.26'),
  ('Neque Associates','Alisa Daniel','1-236-408-8959','fames@aol.net','Germany','32''878''866.24'),
  ('Tellus Suspendisse Limited','Cain Nieves','419-3138','aliquet@aol.com','Germany','90''226''095.48'),
  ('Egestas Lacinia Institute','Savannah Wilder','1-465-613-7646','quis.urna.nunc@protonmail.edu','France','59''383''903.91'),
  ('Sem Pellentesque PC','Basil House','442-0446','at.risus.nunc@hotmail.ca','United States','88''974''524.88'),
  ('Sit Amet LLP','Alfonso Montgomery','1-282-222-5837','auctor.non@protonmail.net','United Kingdom','87''479''802.12'),
  ('Justo Inc.','Madeson Wiley','463-1473','sed.consequat@yahoo.edu','China','43''299''841.96'),
  ('Nunc Commodo Auctor LLC','Quon Dillon','574-5062','amet.ultricies.sem@outlook.ca','France','27''992''344.70'),
  ('Ultricies Dignissim Corp.','Violet Hancock','1-816-778-1385','dapibus@hotmail.net','Germany','67''177''020.03'),
  ('Lacus Varius Et Corp.','Imogene Fuller','1-364-783-0635','nec.enim@protonmail.com','United States','42''538''053.71');
INSERT INTO [Invoices] (company,name,phone,email,country,amount)
VALUES
  ('Tempor Est Institute','Marcia Boyd','1-336-635-4128','mauris@google.couk','Germany','52''543''644.76'),
  ('Neque Et PC','Paul Nash','1-841-824-9605','mauris.vel.turpis@yahoo.net','France','3''820''178.31'),
  ('Sollicitudin A Inc.','Lance Crawford','658-7867','mauris.blandit.enim@aol.edu','United Kingdom','42''540''394.57'),
  ('Eu Consulting','Alan Mays','1-967-384-2207','curae.phasellus@icloud.couk','China','30''657''087.78'),
  ('Ipsum PC','Jermaine Knowles','1-685-462-3598','ut.ipsum@protonmail.net','China','51''664''127.85'),
  ('Convallis LLP','August Head','812-6765','fermentum.convallis.ligula@aol.edu','Germany','36''384''883.97'),
  ('Elit Limited','Michael Gibson','471-9866','posuere@protonmail.org','Germany','95''981''095.92'),
  ('Ornare Egestas Company','Bethany Moreno','1-547-483-6816','duis.risus@protonmail.couk','Italy','44''783''998.54'),
  ('Scelerisque Lorem Foundation','Callum Hutchinson','1-290-551-1342','netus.et.malesuada@outlook.edu','United States','63''652''826.32'),
  ('Libero Proin Mi LLC','Tara Mcneil','203-3292','eget.dictum@yahoo.org','China','61''871''947.99');
