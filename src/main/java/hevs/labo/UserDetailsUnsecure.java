package hevs.labo;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/userDetailsUnsecure")
public class UserDetailsUnsecure extends HttpServlet {
    ResultSet result = null;

    public void init(){
        // Call the correct driver to handle SQL server
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        // The content of the response will be a html page
        response.setContentType("text/html");

        // Get the user input parameter
        String idClient = request.getParameter("idClient");

        try (Connection connection = DriverManager.getConnection(ConnectionDB.getConnectionUrl());
             Statement statement = connection.createStatement()) {

            // Create and execute a SELECT SQL statement.
            String selectSql = "SELECT * from Clients WHERE IdClient = " + idClient;
            System.out.println(selectSql);

            // Will return the result set of the query
            result = statement.executeQuery(selectSql);

            PrintWriter out = response.getWriter();
            out.println("<html><body>");

            while(result.next()){
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(statement.getResultSet().getString(7));
                String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);

                out.println("<p> User n°: " + statement.getResultSet().getString(1) + "</p>");
                out.println("<p> Username : " + statement.getResultSet().getString(3) + "</p>");
                out.println("<p> Firstname : " + statement.getResultSet().getString(4) + "</p>");
                out.println("<p> Lastname : " + statement.getResultSet().getString(5) + "</p>");
                out.println("<p> Gender : " + statement.getResultSet().getString(6) + "</p>");
                out.println("<p> Birthdate : " + formattedDate + "</p>");
                out.println("<p> Email : " + statement.getResultSet().getString(8) + "</p>");
                out.println("<p> Phone : " + statement.getResultSet().getString(9) + "</p>");

                out.println("</br>");
            }
            out.println("<a href=\"index.jsp\">Retry ;)</a>");

            out.println("</body></html>");
        }
        // Handle any errors that may have occurred.
        catch (SQLException | IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // The content of the response will be a html page
        response.setContentType("text/html");

        try (Connection connection = DriverManager.getConnection(ConnectionDB.getConnectionUrl());
             Statement statement = connection.createStatement()) {

            // Execute the store procedure to recreate table Invoices with all data
            String selectSql = "EXEC recreateInvoicesWithData;";

            statement.execute(selectSql);
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
            response.sendError(400, e.getMessage());
        }
    }
}
