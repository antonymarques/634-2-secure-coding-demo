package hevs.labo;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/loginUnsecure")
public class LoginUnsecure extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response){
        // The content of the response will be a html page
        response.setContentType("text/html");

        // Call the correct driver to handle SQL server
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Get the user input parameters
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        ResultSet result;

        try (Connection connection = DriverManager.getConnection(ConnectionDB.getConnectionUrl());
             Statement statement = connection.createStatement();) {

            // Create and execute a SELECT SQL statement
            String selectSql = "SELECT * from Clients WHERE Username = '" + username + "' AND Password = '" + password + "'";
            System.out.println(selectSql);

            // Will return the result set of the query
            result = statement.executeQuery(selectSql);

            PrintWriter out = response.getWriter();
            out.println("<html><body>");

            if(result.next()){
                out.println("<h1>Welcome " + statement.getResultSet().getString(4) + " " + statement.getResultSet().getString(5) + "</h1>");
                out.println("<h2>You are successfully connected. Congrats !</h2>");
            } else {
                out.println("<h1>Sorry, wrong username or password...</h1>");
                out.println("<h2>You should try again !</h2>");
            }
            out.println("<a href=\"index.jsp\">Retry ;)</a>");

            out.println("</body></html>");
        }
        // Handle any errors that may have occurred.
        catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}
