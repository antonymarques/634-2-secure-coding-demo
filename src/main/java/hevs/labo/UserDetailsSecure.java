package hevs.labo;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/userDetailsSecure")
public class UserDetailsSecure extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        // The content of the response will be a html page
        response.setContentType("text/html");

        // Call the correct driver to handle SQL server
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Get the user input parameter
        String idClient = request.getParameter("idClient");

        ResultSet result;

        try (Connection connection = DriverManager.getConnection(ConnectionDB.getConnectionUrl())) {

            // Create and execute a SELECT SQL statement.
            String selectSql = "SELECT * from Clients WHERE IdClient = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(selectSql);
            preparedStatement.setString(1, idClient);

            // Will return the result set of the query
            result = preparedStatement.executeQuery();

            PrintWriter out = response.getWriter();
            out.println("<html><body>");

            if(result.next()){
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(preparedStatement.getResultSet().getString(7));
                String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);

                out.println("<p> User n°: " + preparedStatement.getResultSet().getString(1) + "</p>");
                out.println("<p> Username : " + preparedStatement.getResultSet().getString(3) + "</p>");
                out.println("<p> Firstname : " + preparedStatement.getResultSet().getString(4) + "</p>");
                out.println("<p> Lastname : " + preparedStatement.getResultSet().getString(5) + "</p>");
                out.println("<p> Gender : " + preparedStatement.getResultSet().getString(6) + "</p>");
                out.println("<p> Birthdate : " + formattedDate + "</p>");
                out.println("<p> Email : " + preparedStatement.getResultSet().getString(8) + "</p>");
                out.println("<p> Phone : " + preparedStatement.getResultSet().getString(9) + "</p>");

                out.println("</br>");
            }

            out.println("<a href=\"index.jsp\">Retry ;)</a>");

            out.println("</body></html>");
        }
        // Handle any errors that may have occurred.
        catch (SQLException | IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
