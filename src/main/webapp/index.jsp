<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Secure coding - demo</title>
</head>

<body>
<form method="post">
    <div class="container">
        <label><b>Username</b></label>
        <label>
            <input type="text" placeholder="Enter Username" name="username" required>
        </label>

        <label><b>Password</b></label>
        <label>
            <input type="text" placeholder="Enter Password" name="password" required>
        </label>

        <div class="container-buttons">
            <button type="submit" formaction="loginUnsecure">Login unsecure</button>
            <button type="submit" formaction="loginSecure">Login secure</button>
        </div>


    </div>
</form>

<form method="get">
    <div class="container">
        <label><b>Client's details</b></label>
        <label>
            <input type="text" placeholder="Enter IdClient" name="idClient" required>
        </label>

        <div class="container-buttons">
            <button type="submit" formaction="userDetailsUnsecure">User details unsecure</button>
            <button type="submit" formaction="userDetailsSecure">User details secure</button>
        </div>
    </div>
</form>

<div class="container">
    <button onclick="recreateInvoicesWithData()">Recreate Invoices with data...</button>
</div>

<div id="snackbar">Invoices with data successfully created</div>


<script src="script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</body>
</html>
