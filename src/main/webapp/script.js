function recreateInvoicesWithData() {
    // Get the snackbar DIV
    let x = document.getElementById("snackbar");

    $.ajax({
        type: "post",
        url: "userDetailsUnsecure",
        success: function(){
            x.innerHTML = "Invoices with data successfully created";

            // Add the "show" class to DIV
            x.className = "show";

            // After 3 seconds, remove the show class from DIV
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        },
        error: function(){
            x.innerHTML = "Bad request or invoices with data already existed";

            // Add the "show" class to DIV
            x.className = "show";

            // After 3 seconds, remove the show class from DIV
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
    });
}